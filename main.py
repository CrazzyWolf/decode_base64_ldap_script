import base64
import re
import sys


def start(file_name):
    file = open(file_name, "r", encoding='UTF-8')
    lines = list()
    for line in file:
        lines.append(line)
    file.close()

    # spojení více řádků do jednoho
    temp_lines = list()
    for line in lines:
        if line.startswith(" "):
            temp_lines[-1] = temp_lines[-1].strip() + line[1:]
        else:
            temp_lines.append(line)
    lines = temp_lines

    output_file = open("output.txt", "w", encoding='UTF-8')
    for line in lines:

        if line.startswith("#"):
            continue

        if re.match(r'^\w+:: ', line):
            array = line.split(":: ", 1)
            line_start = array[0]
            line_end = array[1].strip()
            line_end = base64.b64decode(line_end).decode()
            line = line_start + ": " + line_end + "\n"

        print(line, end='')
        output_file.write(line)

    output_file.close()


if __name__ == '__main__':
    start(sys.argv[1])  # main -> edit configurations -> parameters
